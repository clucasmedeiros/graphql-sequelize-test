import { Sequelize } from 'sequelize-typescript';
import { Op } from 'sequelize';

export const sequelize: Sequelize = new Sequelize({
    username: 'admin',
    password: '123456',
    database: 'graphql_blog_development',
    host: '127.0.0.1', 
    dialect: 'mysql',
    operatorsAliases: Op,
    modelPaths: [__dirname + '/models']
})