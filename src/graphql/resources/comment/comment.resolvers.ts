import { GraphQLResolveInfo } from "graphql";
import { Transaction } from "sequelize";
import { User } from "../../../models/User";
import { Comment } from "../../../models/Comment";
import { handleError } from "../../../utils/utils";
import { Post } from "../../../models/Post";
import { sequelize } from "../../../sequelize";

export const commentResolvers = {

    Comment: {

        user: (comment: Comment, args, context, info: GraphQLResolveInfo) => {
            return User
                .findByPk(comment.get('user'))
                .then((user: User) => {
                    return user;
                })
                .catch(handleError);
        },

        post: (comment: Comment, args, context, info: GraphQLResolveInfo) => {
            return Post
                .findByPk(comment.get('post'))
                .then((post: Post) => {
                    return post;
                })
                .catch(handleError);
        },

    },

    Query: {

        //params are: parent, args: {}, context: {}, info
        commentsByPost: (comment: Comment, {postId, first = 10, offset = 0 }, context, info: GraphQLResolveInfo) => {
            postId = parseInt(postId);
            
            return Comment
                .findAll({
                    where: {post: postId},
                    limit: first,
                    offset: offset
                })
                .catch(handleError);
        },

    },

    Mutation: {
        createComment: (parent, {input}, context, info: GraphQLResolveInfo) => {
            return sequelize.transaction((t: Transaction) => {
                return Comment
                    .create(input, {transaction: t});
            }).catch(handleError);
        },

        updateComment: (parent, {id, input}, context, info: GraphQLResolveInfo) => {
            id = parseInt(id);

            return sequelize.transaction((t: Transaction) => {
                return Comment
                    .findByPk(id)
                    .then((comment: Comment) => {
                        if(!comment) throw new Error(`Comment with id ${id} not found!`);

                        return comment.update(input, {transaction: t});
                    });
            }).catch(handleError);
        },

        deleteComment: (parent, {id, input}, context, info: GraphQLResolveInfo) => {
            id = parseInt(id);

            return sequelize.transaction((t: Transaction) => {
                return Comment
                    .findByPk(id)
                    .then((comment: Comment) => {
                        if(!comment) throw new Error(`Comment with id ${id} not found!`);

                        return comment.destroy({transaction: t})
                            .then((comment: any) => !!comment);
                    });
            }).catch(handleError);
        },
    }

}