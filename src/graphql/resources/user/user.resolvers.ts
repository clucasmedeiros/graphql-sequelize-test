import { GraphQLResolveInfo } from "graphql";
import { Transaction } from "sequelize";
import { User } from "../../../models/User";
import { Post } from "../../../models/Post";
import { sequelize } from "../../../sequelize";

export const userResolvers = {

    User: {

        posts: (user: User, { first = 10, offset = 0 }, context, info: GraphQLResolveInfo) => {
            return Post
                .findAll({
                    where: {author: user.get('id')},
                    limit: first,
                    offset: offset
                });
        },

    },

    Query: {

        //params are: parent, args: {}, context: {}, info
        users: (parent, { first = 10, offset = 0 }, context, info: GraphQLResolveInfo) => {
            return User
                .findAll({
                    limit: first,
                    offset: offset
                });
        },

        user: (parent, {id}, context, info: GraphQLResolveInfo) => {
            id = parseInt(id);
            
            return User
                .findByPk(id)
                .then((user: User) => {
                    if(!user) throw new Error(`User with id ${id} not found!`);
                    return user;
                });
        }

    },

    Mutation: {
        createUser: (parent, {input}, context, info: GraphQLResolveInfo) => {
            return sequelize.transaction((t: Transaction) => {
                return User.create(input, {transaction: t});
            });
        },

        updateUser: (parent, {id, input}, context, info: GraphQLResolveInfo) => {
            id = parseInt(id);

            return sequelize.transaction((t: Transaction) => {
                return User.findByPk(id)
                    .then((user: User) => {
                        if(!user) throw new Error(`User with id ${id} not found!`);

                        return user.update(input, {transaction: t});
                    });
            });
        },

        updateUserPassword: (parent, {id, input}, context, info: GraphQLResolveInfo) => {
            id = parseInt(id);

            return sequelize.transaction((t: Transaction) => {
                return User
                    .findByPk(id)
                    .then((user: User) => {
                        if(!user) throw new Error(`User with id ${id} not found!`);

                        return user.update(input, {transaction: t})
                            .then((user: User) => !!user);
                    });
            });
        },

        deleteUser: (parent, {id, input}, context, info: GraphQLResolveInfo) => {
            id = parseInt(id);

            return sequelize.transaction((t: Transaction) => {
                return User
                    .findByPk(id)
                    .then((user: User) => {
                        if(!user) throw new Error(`User with id ${id} not found!`);

                        return user.destroy({transaction: t})
                            .then((user: any) => !!user);
                    });
            });
        },
    }

}