import { GraphQLResolveInfo } from "graphql";
import { Transaction } from "sequelize";
import { Post } from "../../../models/Post";
import { handleError } from "../../../utils/utils";
import { sequelize } from "../../../sequelize";
import { Comment } from "../../../models/Comment";
import { User } from "../../../models/User";

export const postResolvers = {

    Post: {

        author: (post: Post, args, context, info: GraphQLResolveInfo) => {
            return User
                .findByPk(post.get('author'))
                .then((user: User) => {
                    return user;
                })
                .catch(handleError);
        },

        comments: (post: Post, { first = 10, offset = 0 }, context, info: GraphQLResolveInfo) => {
             return Comment
                 .findAll({
                    where: {post: post.get('id')},
                    limit: first,
                    offset: offset
                })
                .catch(handleError);
        },

    },

    Query: {

        //params are: parent, args: {}, context: {}, info
        posts: (parent, { first = 10, offset = 0 }, context, info: GraphQLResolveInfo) => {
            return Post
                .findAll({
                    limit: first,
                    offset: offset
                });
        },

        post: (parent, {id}, context, info: GraphQLResolveInfo) => {
            id = parseInt(id);

            return Post
                .findByPk(id)
                .then((post: Post) => {
                    if(!post) throw new Error(`Post with id ${id} not found!`);
                    return post;
                })
                .catch(handleError);
        }

    },

    Mutation: {
        createPost: (parent, {input}, context, info: GraphQLResolveInfo) => {
            return sequelize.transaction((t: Transaction) => {
                return Post
                    .create(input, {transaction: t});
            }).catch(handleError);
        },

        updatePost: (parent, {id, input}, context, info: GraphQLResolveInfo) => {
            id = parseInt(id);

            return sequelize.transaction((t: Transaction) => {
                return Post
                    .findByPk(id)
                    .then((post: Post) => {
                        if(!post) throw new Error(`Post with id ${id} not found!`);

                        return post.update(input, {transaction: t});
                    });
            }).catch(handleError);
        },

        deletePost: (parent, {id, input}, context, info: GraphQLResolveInfo) => {
            id = parseInt(id);

            return sequelize.transaction((t: Transaction) => {
                return Post
                    .findByPk(id)
                    .then((post: Post) => {
                        if(!post) throw new Error(`Post with id ${id} not found!`);

                        return post.destroy({transaction: t})
                            .then((post: any) => !!post);
                    });
            }).catch(handleError);
        },
    }

}