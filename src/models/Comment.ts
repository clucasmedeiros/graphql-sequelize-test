import { Model, Table, Column, CreatedAt, UpdatedAt, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { User } from './User';
import { Post } from './Post';

@Table({tableName: 'comments', modelName: 'Comment'})
export class Comment extends Model<Comment> {

    @Column
    comment!: string;

    @ForeignKey(() => User)
    user!: number;

    @ForeignKey(() => Post)
    post!: number;

    @CreatedAt
    @Column
    createdAt!: Date;

    @UpdatedAt
    @Column
    updatedAt!: Date;

}