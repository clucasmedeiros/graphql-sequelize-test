import { Model, Table, Column, Unique, NotEmpty, CreatedAt, UpdatedAt, HasMany } from 'sequelize-typescript';
import { Post } from './Post';
import { Comment } from './Comment';

@Table({tableName: 'users', modelName: 'User'})
export class User extends Model<User> {

    @Column
    name!: string;

    @Unique
    @Column
    email!: string;

    @NotEmpty
    @Column
    password!: string;

    @Column
    photo!: Buffer; //Blob

    @HasMany(() => Post)
    posts?: Post[];

    @HasMany(() => Comment)
    comments?: Comment[];

    @CreatedAt
    @Column
    createdAt!: Date;

    @UpdatedAt
    @Column
    updatedAt!: Date;

}