import { Model, Table, Column, CreatedAt, UpdatedAt, BelongsTo, ForeignKey, HasMany } from 'sequelize-typescript';
import { User } from './User';
import { Comment } from './Comment';

@Table({tableName: 'posts', modelName: 'Post'})
export class Post extends Model<Post> {

    @Column
    title!: string;

    @Column
    content!: string;

    @Column
    photo!: Buffer; //Blob

    @ForeignKey(() => User)
    author!: number;

    @HasMany(() => Comment)
    comments?: Comment[];

    @CreatedAt
    @Column
    createdAt!: Date;

    @UpdatedAt
    @Column
    updatedAt!: Date;

}