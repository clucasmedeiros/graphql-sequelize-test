import {createServer} from 'http';
import {sequelize} from './sequelize';
import app from './app';
import { normalizePort, onError, onListening } from './utils/utils';
import { Comment } from './models/Comment';

const port = normalizePort(process.env.port || 5555);
const server = createServer(app)

sequelize.sync()
.then(() => {
    server.listen(port);
    server.on('error', onError(server));
    server.on('listening', onListening(server));
}).catch(err => {
    console.log(err);
})