mutation {
  createPost(input: {
    title: "Teste post",
    content: "Conteúdo do post",
    photo: "https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwj4m9ah7YHiAhWSJ7kGHSr4DXoQjRx6BAgBEAU&url=https%3A%2F%2Fwww.youtube.com%2Fchannel%2FUC_W0_Nv-3rxpWVvosddhrPw&psig=AOvVaw18ILVLqixjLD64Ww91UDP5&ust=1557058548786929",
    author: 1
  }) {
    id
  }
}

mutation {
  createUser(input: {
    name: "Lucas",
    email: "teste@gmail.com",
    password: "1234"
  }) {
    id
  }
}

mutation {
  createComment(input: {
    comment: "comentário cagado 2",
    post:1
    user:1
  }){
    id
  }
}

query{
  users {
    id
    name
    posts{
      id
      content
      comments{
        comment
      }
    }
  }
}

query{
  posts{
    comments{
      id
      user{
        name
        posts{
          comments{
            id
          }
        }
      }
    }
  }
}
